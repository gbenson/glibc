/* Get address of thread local variable.
   Copyright (C) 2002-2018 Free Software Foundation, Inc.
   This file is part of the GNU C Library.
   Contributed by Ulrich Drepper <drepper@cygnus.com>, 2002.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include <link.h>
#include "thread_dbP.h"


/* Get the TLS module ID from the `struct link_map' in the inferior.  */
#ifdef WITH_INFINITY_LIBTHREAD_DB
static td_err_e
infinity_get_tls_modid (const td_thrhandle_t *th, psaddr_t map_address,
			psaddr_t *modid_p)
{
  union i8x_value args[1], rets[1];

  args[0].p = map_address;

  CALL_INFINITY (th->th_ta_p, link_map_get_tls_modid, args, rets);

  *modid_p = rets[0].p;

  return TD_OK;
}
#endif /* WITH_INFINITY_LIBTHREAD_DB */


/* Get the TLS module ID from the `struct link_map' in the inferior.  */
static td_err_e
get_tls_modid (const td_thrhandle_t *th, psaddr_t map_address,
	       psaddr_t *modid_p)
{
  td_err_e err;
  psaddr_t modid;

  /* Use Infinity variant if applicable.  */
#ifdef WITH_INFINITY_LIBTHREAD_DB
  if (ta_using_infinity (th->th_ta_p))
    return infinity_get_tls_modid (th, map_address, modid_p);
#endif

  err = DB_GET_FIELD (modid, th->th_ta_p, map_address, link_map,
		      l_tls_modid, 0);
  if (err == TD_NOCAPAB)
    return TD_NOAPLIC;

  if (err == TD_OK)
    *modid_p = modid;

  return err;
}


/* Get address of thread local variable.  */
td_err_e
td_thr_tls_get_addr (const td_thrhandle_t *th,
		     psaddr_t map_address, size_t offset, psaddr_t *address)
{
  td_err_e err;
  psaddr_t modid = NULL;

  /* Get the module ID.  */
  err = get_tls_modid (th, map_address, &modid);
  if (err != TD_OK)
    return err;

  /* Get the base address.  */
  err = td_thr_tlsbase (th, (uintptr_t) modid, address);
  if (err != TD_OK)
    return err;

  /* Add the offset.  */
  *address += offset;

  return TD_OK;
}
