/* Thread debugging using Infinity functions.
   Copyright (C) 2017 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include <assert.h>
#include <stdbool.h>

#include "thread_dbP.h"

#pragma weak ps_foreach_infinity_note
#pragma weak ps_get_register
#pragma weak ps_get_thread_area


/* Relocation information for an Infinity note.  */
struct td_relocinfo
{
  /* Function to relocate addresses in this note.  */
  ps_infinity_reloc_f *reloc_func;

  /* Value to be passed as the first argument of RELOC_FUNC.  */
  void *reloc_func_arg;
};


/* Import a single note, creating the context if necessary.  */
static ps_err_e
td_ta_import_note (void *ta_p, const char *buf, size_t buflen,
		   const char *srcname, ssize_t srcoffset,
		   ps_infinity_reloc_f *rf, void *rf_arg)
{
  td_thragent_t *ta = (td_thragent_t *) ta_p;
  i8x_err_e err;

  if (ta->i8_ctx == NULL)
    {
      err = i8x_ctx_new (0, NULL, &ta->i8_ctx);

      if (err != I8X_OK)
	return PS_ERR;
    }

  struct td_relocinfo *ri = calloc (1, sizeof (struct td_relocinfo));
  if (ri == NULL)
    return PS_ERR;

  struct i8x_func *func;
  err = i8x_ctx_import_bytecode (ta->i8_ctx, buf, buflen, srcname,
				 srcoffset, &func);
  if (err != I8X_OK)
    {
      free (ri);

      if (err == I8X_NOTE_CORRUPT
	  || err == I8X_NOTE_INVALID
	  || err == I8X_NOTE_UNHANDLED)
	return PS_OK;

      return PS_ERR;
    }

  ri->reloc_func = rf;
  ri->reloc_func_arg = rf_arg;
  i8x_func_set_userdata (func, ri, free);

  func = i8x_func_unref (func);

  return PS_OK;
}


/* Load and register Infinity functions from the process.  */
static td_err_e
td_ta_import_notes (td_thragent_t *ta)
{
  if (&ps_foreach_infinity_note == NULL)
    return TD_NOCAPAB;

  if (ps_foreach_infinity_note (ta->ph, td_ta_import_note, ta) != PS_OK)
    return TD_ERR;
  if (ta->i8_ctx == NULL)
    return TD_NOCAPAB;  /* No notes found.  */

  return TD_OK;
}


/* Infinity native function "procservice::getpid()i".  */
static i8x_err_e
td_ps_getpid (struct i8x_xctx *xctx, struct i8x_inf *inf,
	      struct i8x_func *func, union i8x_value *args,
	      union i8x_value *rets)
{
  td_thragent_t *ta = (td_thragent_t *) i8x_inf_get_userdata (inf);

  rets[0].i = ps_getpid (ta->ph);

  return I8X_OK;
}


/* Infinity native function "procservice::get_register(ii)ii".  */
static i8x_err_e
td_ps_get_register (struct i8x_xctx *xctx, struct i8x_inf *inf,
		    struct i8x_func *func, union i8x_value *args,
		    union i8x_value *rets)
{
  td_thragent_t *ta = (td_thragent_t *) i8x_inf_get_userdata (inf);
  lwpid_t lwpid = args[0].i;
  int regnum = args[1].i;

  assert (&ps_get_register != NULL);
  rets[1].i = ps_get_register (ta->ph, lwpid, regnum, &rets[0].p);

  return I8X_OK;
}


/* Infinity native function "procservice::get_thread_area(ii)ip".  */
static i8x_err_e
td_ps_get_thread_area (struct i8x_xctx *xctx, struct i8x_inf *inf,
		       struct i8x_func *func, union i8x_value *args,
		       union i8x_value *rets)
{
  td_thragent_t *ta = (td_thragent_t *) i8x_inf_get_userdata (inf);
  lwpid_t lwpid = args[0].i;
  int index = args[1].i;

  assert (&ps_get_thread_area != NULL);
  rets[1].i = ps_get_thread_area (ta->ph, lwpid, index, &rets[0].p);

  return I8X_OK;
}


/* Address relocation function for nptl_db inferiors.  */
static i8x_err_e
td_relocate_address (struct i8x_inf *inf, struct i8x_reloc *reloc,
		     uintptr_t *result)
{
  struct i8x_func *func = i8x_reloc_get_func (reloc);
  struct td_relocinfo *ri
    = (struct td_relocinfo *) i8x_func_get_userdata (func);

  if (ri->reloc_func (ri->reloc_func_arg,
		      (psaddr_t) i8x_reloc_get_unrelocated (reloc),
		      (psaddr_t *) result) != PS_OK)
    return I8X_RELOC_FAILED;

  return I8X_OK;
}


/* Memory reader function for nptl_db inferiors.  */
static i8x_err_e
td_read_memory (struct i8x_inf *inf, uintptr_t addr, size_t len,
		void *result)
{
  td_thragent_t *ta = (td_thragent_t *) i8x_inf_get_userdata (inf);

  if (ps_pdread (ta->ph, (psaddr_t) addr, result, len) != PS_OK)
    return I8X_READ_MEM_FAILED;

  return I8X_OK;
}


/* Macros for td_ta_init_infinity.  */
#define REGISTER_NATIVE(sig, impl)					\
  do {									\
    i8x_err_e __err = i8x_ctx_import_native (ta->i8_ctx, sig,		\
					     impl, NULL);		\
    if (__err != I8X_OK)						\
      return __err;							\
  } while (0)

#define STORE_FUNCREF(provider, name, args, rets)			\
  do {									\
    i8x_err_e __err = i8x_ctx_get_funcref (ta->i8_ctx,			\
					   #provider "::" #name		\
					   "(" args ")" rets,		\
					   &ta->i8_fn_ ## provider	\
					   ## _ ## name);		\
    if (__err != I8X_OK)						\
      return __err;							\
  } while (0)

#define STORE_TDB_FUNCREF(name, args, rets)				\
  do {									\
    STORE_FUNCREF (thread, name, args, rets);				\
									\
    if (i8x_funcref_is_resolved (ta->i8_fn_thread ## _ ## name))	\
      got_thread_db_funcs = true;					\
  } while (0)


/* Initialize agent descriptor for operation using Infinity functions,
   Sets up all Infinity-specific fields except TA->i8_ctx which should
   be supplied preloaded with all Infinity functions from the process
   we're operating on.  Returns I8X_OK upon success, TD_NOCAPAB if no
   relevant Infinity functions were found, or another i8x_err_e code
   if something went wrong.  */
static i8x_err_e
td_ta_init_infinity (td_thragent_t *ta)
{
  bool got_thread_db_funcs = false;
  struct i8x_func *func;
  i8x_err_e err;

  assert (ta->i8_ctx != NULL);

  /* Register the native functions we provide.  */
  REGISTER_NATIVE ("procservice::getpid()i",  td_ps_getpid);

  if (&ps_get_register != NULL)
    REGISTER_NATIVE ("procservice::get_register(ii)ii",
		     td_ps_get_register);

  if (&ps_get_thread_area != NULL)
    REGISTER_NATIVE ("procservice::get_thread_area(ii)ip",
		     td_ps_get_thread_area);

  /* Store references to thread-debugging functions we use.  */
  STORE_TDB_FUNCREF (from_lwpid,	    "i",	"ip");
  STORE_TDB_FUNCREF (iterate,		    "Fi(po)oi",	"i");
  STORE_TDB_FUNCREF (get_event,		    "pi",	"ii");
  STORE_TDB_FUNCREF (get_lwpid,		    "p",	"ii");
  STORE_TDB_FUNCREF (get_priority,	    "p",	"ii");
  STORE_TDB_FUNCREF (get_report_events,	    "p",	"ii");
  STORE_TDB_FUNCREF (get_specific,	    "p",	"ip");
  STORE_TDB_FUNCREF (get_start_routine,	    "p",	"ip");
  STORE_TDB_FUNCREF (get_state,		    "p",	"ii");
  STORE_TDB_FUNCREF (get_tlsbase,	    "pi",	"ip");

  if (!got_thread_db_funcs)
    return I8X_UNRESOLVED_FUNCTION;  /* Maps to TD_NOCAPAB.  */

  /* Store references to other Infinity functions we use.  */
  STORE_FUNCREF (link_map, get_tls_modid,   "p",	"i");

  /* Register the callback wrapper for td_ta_thr_iter.  */
  err = i8x_ctx_import_native (ta->i8_ctx, "::thr_iter_cb(po)i",
			       _td_ta_infinity_thr_iter_cb, &func);
  if (err != I8X_OK)
    return err;

  ta->i8_thr_iter_cb = i8x_funcref_ref (i8x_func_get_funcref (func));
  func = i8x_func_unref (func);

  /* Create the inferior.  */
  err = i8x_inf_new (ta->i8_ctx, &ta->i8_inf);
  if (err != I8X_OK)
    return err;

  i8x_inf_set_userdata (ta->i8_inf, ta, NULL);
  i8x_inf_set_relocate_fn (ta->i8_inf, td_relocate_address);
  i8x_inf_set_read_mem_fn (ta->i8_inf, td_read_memory);

  /* Create an execution context.  */
  err = i8x_xctx_new (ta->i8_ctx, 512, &ta->i8_xctx);
  if (err != I8X_OK)
    return err;

  return I8X_OK;
}
#undef REGISTER_NATIVE
#undef STORE_FUNCREF
#undef STORE_TDB_FUNCREF


/* Attempt to initialize TA for operation using Infinity functions.
   Returns TD_OK if Infinity functions should be used, TD_NOCAPAB
   if Infinity support is unavailable, or another td_err_e code if
   something went wrong.  */
td_err_e
_td_ta_init_infinity (td_thragent_t *ta)
{
  td_err_e err = td_ta_import_notes (ta);

  if (err == TD_OK)
    err = _td_err_from_i8x_err (td_ta_init_infinity (ta));

  if (err != TD_OK)
    _td_ta_delete_infinity (ta);

  return err;
}


/* Macro for _td_ta_delete_infinity.  */
#define TA_UNREF(type, ref) \
  ((void) (ref = i8x_ ## type ## _unref (ref)))


/* Deallocate Infinity-specific fields in TA and set all to NULL.  */
void
_td_ta_delete_infinity (td_thragent_t *ta)
{
  TA_UNREF (funcref, ta->i8_fn_link_map_get_tls_modid);
  TA_UNREF (funcref, ta->i8_fn_thread_from_lwpid);
  TA_UNREF (funcref, ta->i8_fn_thread_iterate);
  TA_UNREF (funcref, ta->i8_fn_thread_get_event);
  TA_UNREF (funcref, ta->i8_fn_thread_get_lwpid);
  TA_UNREF (funcref, ta->i8_fn_thread_get_priority);
  TA_UNREF (funcref, ta->i8_fn_thread_get_report_events);
  TA_UNREF (funcref, ta->i8_fn_thread_get_specific);
  TA_UNREF (funcref, ta->i8_fn_thread_get_start_routine);
  TA_UNREF (funcref, ta->i8_fn_thread_get_state);
  TA_UNREF (funcref, ta->i8_fn_thread_get_tlsbase);

  TA_UNREF (funcref, ta->i8_thr_iter_cb);

  TA_UNREF (xctx, ta->i8_xctx);
  TA_UNREF (inf, ta->i8_inf);

  TA_UNREF (ctx, ta->i8_ctx);
}
#undef TA_UNREF


/* Map a libi8x error code to a libthread_db one.  */
td_err_e
_td_err_from_i8x_err (i8x_err_e err)
{
  switch (err)
    {
    case I8X_OK:
      return TD_OK;

    case I8X_ENOMEM:
      return TD_MALLOC;

    case I8X_UNRESOLVED_FUNCTION:
      return TD_NOCAPAB;

    default:
      return TD_ERR;
    }
}
