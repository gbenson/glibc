# -*- coding: utf-8 -*-
# Copyright (C) 2016-17 Free Software Foundation, Inc.
#  This file is part of the GNU C Library.
#
#  The GNU C Library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
#
#  The GNU C Library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with the GNU C Library; if not, see
#  <http://www.gnu.org/licenses/>.  */

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from i8c.runtime import TestCase

TestCase.import_builtin_constants()
TestCase.import_constants_from("infinity-nptl-constants.h")
TestCase.import_constants_from("infinity-nptl_db-constants.h")

class TestThrGetState(TestCase):
    TESTFUNC = "thread::get_state(p)ii"

    def __do_test(self, descr, expect_state):
        result = self.call(self.TESTFUNC, descr)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], TD_OK)
        self.assertEqual(result[1], expect_state)

    def test_faked(self):
        """Test thread::get_state with a faked descriptor"""
        self.__do_test(NULL, TD_THR_ACTIVE)

    def __do_test_real(self, cancelhandling, expect_state):
        with self.memory.builder() as mem:
            descr = mem.alloc()
            descr.store_i32(PTHREAD_CANCELHANDLING_OFFSET,
                            cancelhandling)
        self.__do_test(descr, expect_state)

    def test_active(self):
        """Test thread::get_state with an active thread"""
        self.__do_test_real(0, TD_THR_ACTIVE)

    def test_exiting(self):
        """Test thread::get_state with an exiting thread"""
        self.__do_test_real(EXITING_BITMASK, TD_THR_UNKNOWN)

    def test_exited(self):
        """Test thread::get_state with an exited thread"""
        self.__do_test_real(EXITING_BITMASK | TERMINATED_BITMASK,
                            TD_THR_ZOMBIE)
