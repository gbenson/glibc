/* Which thread is running on an LWP?
   Copyright (C) 2003-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */


/* Given an lwpid_t identifying an LWP, return TD_OK and the
   descriptor of the thread running on it, or a non-TD_OK
   td_err_e code indicating the reason for failure and an
   undefined value that must be ignored.  The caller must
   ensure that __pthread_initialize_minimal has gotten far
   enough; see the comments in infinity_map_lwp2thr.i8 for
   specifics.  */

define libpthread::__lookup_th_unique returns td_err_e, pthread_t
	argument lwpid_t lwpid

#ifdef __s390x__
  /* On S/390x the thread pointer is split across two
     32-bit registers when running in 64-bit mode.  */
	dup
	load I8_TS_REGISTER
	call procservice::get_register
	bne PS_OK, get_register_failed
	shl 32
	swap
	load I8_TS_REGISTER
	add 1
	call procservice::get_register
	bne PS_OK, get_register_failed
	or
	cast 0, ptr
#else
  /* Not s390x.  */
	load I8_TS_REGISTER
	call procservice::get_register
	cast 1, ptr
	bne PS_OK, get_register_failed
#endif
	add I8_TS_REG_BIAS
	load TD_OK
	return

get_register_failed:
#ifdef __s390x__
	cast 0, ptr
#endif
	load TD_ERR
	return
