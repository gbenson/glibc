/* Common definitions for NPTL Infinity functions.
   Copyright (C) 2015-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#ifndef _INFINITY_NPTL_I8
#define _INFINITY_NPTL_I8 1

#include "infinity-common.i8"
#include "infinity-nptl-constants.h"
#include "infinity-nptl_db-constants.h"

/* Lightweight process IDs.  */
typedef pid_t lwpid_t

/* Error codes defined in thread_db.h and proc_service.h.  */
typedef int32_t td_err_e
typedef int32_t ps_err_e

/* A pointer to a struct pthread.  */
typedef ptr pthread_t

/* Possible thread states.  */
typedef int32_t td_thr_state_e

/* Callback for iteration over threads.  */
typedef func int (pthread_t, opaque) thr_iter_f

/* The two thread lists.  */
extern ptr __stack_user
extern ptr stack_used

/* Return the PID of the process being accessed.  */
extern func pid_t () procservice::getpid

/* Return the value stored in the specified register of the specified
   LWP.  The "int" parameter specifies a DWARF register number.  */
extern func ps_err_e, int (lwpid_t, int) procservice::get_register

/* Return the special per-thread address associated with the specified
   LWP.  Not available on all platforms.  The meaning of the "int"
   parameter is machine-dependent. */
extern func ps_err_e, ptr (lwpid_t, int) procservice::get_thread_area

#endif /* infinity-nptl-private.i8 */
