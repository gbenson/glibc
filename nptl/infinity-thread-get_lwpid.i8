/* Get the LWPID of a thread.
   Copyright (C) 2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include "infinity-nptl-private.i8"

/* Return the LWPID of a thread.  */

define thread::get_lwpid returns td_err_e, lwpid_t
	argument pthread_t descr

	dup
	beq NULL, fake_descriptor

real_descriptor:
	add PTHREAD_TID_OFFSET
	deref pid_t
	load TD_OK
	return

fake_descriptor:
	call procservice::getpid
	load TD_OK
	return
