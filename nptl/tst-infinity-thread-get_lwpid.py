# -*- coding: utf-8 -*-
# Copyright (C) 2016-17 Free Software Foundation, Inc.
#  This file is part of the GNU C Library.
#
#  The GNU C Library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
#
#  The GNU C Library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with the GNU C Library; if not, see
#  <http://www.gnu.org/licenses/>.  */

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from i8c.runtime import TestCase

TestCase.import_builtin_constants()
TestCase.import_constants_from("infinity-nptl-constants.h")
TestCase.import_constants_from("infinity-nptl_db-constants.h")

class TestThrGetLWPID(TestCase):
    TESTFUNC = "thread::get_lwpid(p)ii"
    MAIN_PID = 12345

    def setUp(self):
        self.ps_getpid_called = False

    @TestCase.provide("procservice::getpid()i")
    def __ps_getpid(self):
        self.ps_getpid_called = True
        return self.MAIN_PID

    def __do_test(self, descr, expect_lwpid):
        result = self.call(self.TESTFUNC, descr)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], TD_OK)
        self.assertEqual(result[1], expect_lwpid)

    def test_faked(self):
        """Test thread::get_lwpid with a faked descriptor"""
        self.__do_test(NULL, self.MAIN_PID)
        self.assertTrue(self.ps_getpid_called)

    def test_real(self):
        """Test thread::get_lwpid with a real descriptor"""
        lwpid = self.MAIN_PID - 5
        with self.memory.builder() as mem:
            descr = mem.alloc()
            descr.store_i32(PTHREAD_TID_OFFSET, lwpid)
        self.__do_test(descr, lwpid)
        self.assertFalse(self.ps_getpid_called)
