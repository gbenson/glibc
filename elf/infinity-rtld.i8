/* Exported definitions for runtime linker Infinity functions.
   Copyright (C) 2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#ifndef _INFINITY_RTLD_I8
#define _INFINITY_RTLD_I8 1

#include "infinity-common.i8"

/* A TLS module ID.  */
typedef size_t tls_modid_t

/* A pointer to a struct link_map.  */
typedef ptr link_map_t

/* A pointer to a struct dtv_slotinfo.  */
typedef ptr dtv_slotinfo_t

/* Special values returned by link_map::get_tls_offset.  Note
   that these do not necessarily correspond to the values used
   in C code defined in tls.h and/or link.h.  We have to define
   our own values to avoid baking rtld-specific constants into
   libpthread.so notes.  */
#define NO_TLS_OFFSET			-1
#define FORCED_DYNAMIC_TLS_OFFSET	-2

/* Return the DTV slotinfo list entry for the specified TLS module ID.
   Return NULL if the specified module was not found in the list.  */
extern func dtv_slotinfo_t (tls_modid_t modid) dtv_slotinfo::__for_modid

/* Return the TLS generation of the specified DTV slotinfo list entry.  */
extern func size_t (dtv_slotinfo_t) dtv_slotinfo::__get_generation

/* Return the link map of the specified DTV slotinfo list entry.  */
extern func link_map_t (dtv_slotinfo_t) dtv_slotinfo::__get_link_map

/* Return the TLS module ID of the specified link map.  */
extern func tls_modid_t (link_map_t) link_map::get_tls_modid

/* Return the offset into the static TLS block of the specified link
   map, or one of the negative values defined above if this link map
   is not using static TLS.  */
extern func ptrdiff_t (link_map_t) link_map::__get_tls_offset

#endif /* infinity-rtld.i8 */
